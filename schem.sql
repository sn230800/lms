CREATE TABLE `users` (
	`id`	int NOT NULL,
	`full_name`	char ( 150 ) NOT NULL,
	`email`	char ( 150 ),
	`passwordd`	char ( 50 ),
	`verification_code`	char ( 50 ) NOT NULL,
	`status`	TEXT DEFAULT 'студент',
	PRIMARY KEY(`id`)
);

CREATE TABLE `users_profile` (
	`id`	int NOT NULL UNIQUE,
	`telephone_number`	int ( 30 ),
	`native_city`	char ( 150 ),
	`self_info`	char ( 300 ),
	`vk`	char ( 150 ),
	`facebook`	char ( 150 ),
	`linkedin`	char ( 150 ),
	`instagram`	char ( 150 ),
	FOREIGN KEY(`id`) REFERENCES `users`(`id`),
	FOREIGN KEY(`id`) REFERENCES `education_group`(`id_group`),
	PRIMARY KEY(`id`)
);

CREATE TABLE `students` (
	`id`	int NOT NULL UNIQUE,
	`group_number`	int,
	`year_of_admission`	int,
	`academic_degree`	TEXT DEFAULT 'бакалавр',
	`form_of_education`	INTEGER DEFAULT 'очная',
	`basis_of_education`	INTEGER DEFAULT 'бюджетная',
	FOREIGN KEY(`id`) REFERENCES `users`(`id`),
	PRIMARY KEY(`id`)
);

CREATE TABLE `education_group` (
	`id_group`	int NOT NULL UNIQUE,
	`group_name`	char ( 150 ),
	`faculty_name`	char ( 150 ),
	`course_number`	char ( 30 ),
	PRIMARY KEY(`id_group`)
);

CREATE TABLE `education_course` (
	`id_course`	int NOT NULL UNIQUE,
	`course_name`	char ( 150 ),
	`description`	char ( 1000 ),
	`course_materials`	char ( 150 ),
	`available_homework`	char ( 1000 ),
	`list_of_course_instructors`	TEXT,
	`list_of_confidants`	TEXT,
	PRIMARY KEY(`id_course`)
);

CREATE TABLE `material` (
	`name`	char ( 150 ),
	`text`	char ( 10000 ),
	`date_added`	timestamp
);

CREATE TABLE `homework` (
	`name`	char ( 150 ),
	`from_time`	timestamp,
	`until_time`	timestamp,
	`task`	char ( 1000 )
);
