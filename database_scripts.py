import sqlite3 as sq

# Параметры базы данных
DATABASE = 'database.db'
SCHEME = 'schem.sql'

items = [ 
  (1, "Ленина Елена Денисовна", "lenin@yandex.ru", "123456789", "sobaka", "студент"),
  (2, "Приходько Борис Анатольевич", "prihh@gmail.com", "mamalama", "animal", "студент"),
  (3, "Конов Борис Викторович", "konbor@gmail.com", "kristall", "horse", "студент"),
  (4, "Медведова Анна Владимировна", "medved@yandex.ru", "koshka", "ulitka", "преподаватель"),
  (5, "Беликова Виктория Вячеславовна", "belik@yandex.ru", "happy", "flower", "преподаватель"),
  (6, "Рудова Татьяна Алексеевна", "", "", "coffee", "студент"),
  (7, "Афанасьева Ирина Алексеевна", "", "", "road", "студент"),
  (8, "Говорунов Михаил Борисович", "", "", "doll", "студент"),
  (9, "Попов Анатолий Александрович", "", "", "billion", "преподаватель"),
  (10, "Пугина Наталия Александровна", "", "", "money", "преподаватель"),
]


def insert_testdata():
    connection = connect_db()
    cursor = connection.cursor()
    cursor.executemany("INSERT INTO users VALUES(?, ?, ?, ?, ?, ?)", items)
    connection.commit()
    connection.close()

# Подключение базы данных
def connect_db():
    return sq.connect(DATABASE)


# Создание базы данных (по скрипту из SCHEME)
def create_db():
    connection = connect_db()
    cursor = connection.cursor()
    with open(SCHEME, mode='r') as file:
        scheme_script = file.read()
    cursor.executescript(scheme_script)
    connection.commit()
    cursor.close()
    connection.close()


# Вспомогательная функция для упрощения получения результатов по ключу
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def select_users(user_id=None):
    my_list = []
    connection = connect_db()

    if user_id == None:
        sql = "SELECT * FROM users"
    else:
        sql = f"SELECT * FROM users WHERE id = {user_id}"

    try:
        cursor = connection.execute(sql)
        cursor.row_factory = dict_factory
        for row in cursor:
            record = {
                'id': row.get('id'),
		'full_name': row.get('full_name'),
		'email': row.get('email'),
		'passwordd': row.get('passwordd'),
		'verification_code': row.get('verification_code'),
		'status': row.get('status'),
            }
            my_list.append(record)

    except connection.Error as error:
        print("Error connection to database", error)
    finally:
        if connection: connection.close()

    return my_list
