from fastapi import FastAPI
import controller as controller

tags_metadata = [
    {
        'name': 'users',
        'description': 'users',
    }
]

app = FastAPI(
    title='Mini api',
    description='This is my mini api (description)',
    version='1.0.0',
    openapi_tags=tags_metadata
)

@app.get("/")
async def root():
    return controller.get_users()

@app.get("/{item_id}")
async def read_user(user_id: int):
    return controller.get_user(user_id)
    
