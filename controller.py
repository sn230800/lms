import database_scripts as db_scripts

def get_users():
    return db_scripts.select_users()

def get_user(user_id):
    results = db_scripts.select_users(user_id)
    return results
